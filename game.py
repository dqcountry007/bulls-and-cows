from random import randint

#bull is a correct number
# cow is the correct number, in the wrong spot 

# Generates a list of four, unique, single-digit numbers
def generate_secret():
    # TODO:
    # You need to delete the word "pass" and write your
    # function, here.
    my_list = []
    while len(my_list) < 4:
        #n = number 
        #skips the first number, and uses the second
        #so 1,2,3,4,5,6,7,8,9
        n = randint(0,9)
        if n not in my_list:
            my_list.append(n)
    return my_list

#split the numbers into a readable list
#parse means to extract information from a string. 
#turns a string1234 into a list1234
# s = string of numbers
# l = list
def parse_numbers(s):
    l = []
    for n in s:
        number = int(n)
        l.append(number)
        print("l: ", l)
        #prints l: [1,2,3,4]
        #print("s: ", s)
        #prints s: 1234
    return l 

#first list, second list
# need to compare the lists to see if our guess and the secret code match
def count_exact_matches(first, second):
    count = 0
    for a,b in zip(first,second):
        if a == b:
            count = count + 1
    return count

def count_common_entries(first, second):
    count = 0
    for entry in first:
        if entry in second:
            count = count + 1
    return count




def play_game():
    print("Let's play Bulls and Cows!")
    print("--------------------------")
    print("I have a secret four numbers.")
    print("Can you guess it?")
    print("You have 20 tries.")
    print()

    secret = generate_secret()
    print("secret: ", secret)
    number_of_guesses = 20

    for i in range(number_of_guesses):
        prompt = "Type in guess #" + str(i + 1) + ": "
        guess = input(prompt)

        # TODO:
        # while the length of their response is not equal
        # to 4,
        #   tell them they must enter a four-digit number
        #   prompt them for the input, again
        while len(guess) != 4:
            print("you must enter a four-digit number")
            guess = input(prompt)

        # TODO:
        # write a function and call it here to:
        # convert the string in the variable guess to a list
        # of four single-digit numbers

        converted_guess = parse_numbers(guess)

        # TODO:
        # write a function and call it here to:
        # count the number of exact matches between the
        # converted guess and the secret
        #converted guess is the result of parse_numbers(guess) above
        #secret is the list of numbers containing the secret code
        total_exact_matches = count_exact_matches(converted_guess, secret)
        

        # TODO:
        # write a function and call it here to:
        # count the number of common entries between the
        # converted guess and the secret
        total_common_entries = count_common_entries(converted_guess,secret)
        print("total_common_entries: ", total_common_entries)
        

        # TODO:
        # if the number of exact matches is four, then
        # the player has correctly guessed! tell them
        # so and return from the function.

        print("total_exact__matches: ", total_exact_matches)

        if total_exact_matches == 4:
            print("you've guessed the right numbers!!")

        # TODO:
        # report the number of "bulls" (exact matches)
        print("total number of bulls: ", total_exact_matches)

        # TODO:
        # report the number of "cows" (common entries - exact matches)
        print("total number of cows: ", total_common_entries - total_exact_matches)

    # TODO:
    # If they don't guess it in 20 tries, tell
    # them what the secret was.
    print("the secret was: ", secret)


# Runs the game
def run():
    # TODO: Delete the word pass, below, and have this function:
    # Call the play_game function
    play_game()
    response = input("Do you want to play a game?")
    
    # While the person wants to play the game
    #   Call the play_game function
    #   Ask if they want to play again
    #pass
    while response == "Y":
        play_game()
        response = input("Do you want to play again?")


if __name__ == "__main__":
    run()
